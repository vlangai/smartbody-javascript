function allReady(){
if(scene != null){	 
     console.log("|--------------------------------------------|")
     console.log("|            Starting test Demo              |")
     console.log("|--------------------------------------------|")

	 scene.setDrawDebug(true);
 
     defaultViewer.defaultViewer();
	 console.log("Configuring scene parameters and Camera")
     var camera = Module.getCamera()
	 camera.setEye(1, 6.8, 20.6)
	 camera.setCenter(.5, 1.5, -1)
	 camera.setUpVector(new Module.SrVec(0, 1, 0))
	 camera.setScale(1)
	 camera.setFov(0.4)
	 camera.setFarPlane(10000)
	 camera.setNearPlane(0.01)
	 camera.setAspectRatio(1.333333)
	 defaultInterface.defaultInterface();
	var jointMapManager = scene.getJointMapManager()
	zebra2Map = jointMapManager.getJointMap("zebra2")
    if (zebra2Map == 'None'){
		zebra2Map = jointMapManager.createJointMap('zebra2')
	}
		var BallDemo = Module.SBScript.extend('SBScript', {
			update : function(time){				

				
				var allobjects = ["light0", "light1", "camera"]
				for(var i=0;i< allobjects.length;i++)
				{
					var x = Math.sin(time + .5 * i)
					var y = Math.cos(time + .5 * i)
					var obj = scene.getPawn(allobjects[i])
					var pos = obj.getPosition()

					pos.setData(0, x * 1.0)
					pos.setData(2, y * 1.0)
					obj.setPosition(pos)
				}

			}
		});
	// Run the update script
	var balldemo = new BallDemo
	// Add asset paths
	scene.loadAssetsFromPath("mesh/ChrBrad")
	scene.loadAssetsFromPath("mesh/ChrRachel")
	scene.addAssetPath('script', 'scripts')
	scene.addAssetPath('mesh', 'mesh')
	scene.addAssetPath('motion', 'ChrBrad')
	scene.addAssetPath('motion', 'ChrRachel')
	scene.addAssetPath("script", "behaviorsets")
	scene.loadAssets()
	scene.addScript('balldemo', balldemo)
	
		
 }else{
      console.log("SBScene does not exist");
 }
}